# Gotham city - Home website hosting and more

This is my home-lab... sort of...  it runs on an old synology nas.  Whats missing from this
is the VM setup which is just done on the nas, and network static IP's and port forwarding. 
Nginx is setup so everything goes from 80 to 443 automatically.  443 is encrypted with 
LetsEncypt certificates.

## VM's
I'm running HomeAssistant on a vm, bu there's no info for it here.

## Docker Containers

Can view the docker-compose file [here](./infra/docker-compose.yaml).

 - nginx 
    - reverse proxy and static hosting and managing SSL
    - see the config [here](./infra/nginx/4-10.us.conf). 
    - https://nginx.org/en/
    - proxy for:
        - matomo 
        - frontend
        - backend
        - homeassistant
        - synology nas
 - certbot 
    - automatically updates ssl certificates with LetsEncrypt
    - https://certbot.eff.org/
 - matomo 
    - just used to watch what traffic I get 
    - https://matomo.org/
 - frontend
    - this is the angular app in the sites/apps/frontend directory. this isn't a docker container. The
    static files are hosted by nginx.
 - backend 
    - this is the express project app in the sites/apps/backend directory.  this is also deployed as 
    static files, but a node.js docker container uses that directory when it launches.
 - postgresql
    - used by the backend app
    - https://www.postgresql.org/
 - pihole
    - used in my home to filter network traffic
    - https://pi-hole.net/
 - mariadb
    - used by Matomo
    - https://mariadb.org/
 - unbound
    - home DNS server used by Pi-hole
    - https://www.unbound.org/
 - net alert x
    - home network monitoring
    - https://netalertx.com/

## IP's

 - 192.168.68.2  - NAS reserved
 - 192.168.68.3  - NAS
 - 192.168.68.4  - camera 1
 - 192.168.68.5  - camera 2
 - 192.168.68.6  - open (reset router)
 - 192.168.68.7  - pi-hole
 - 192.168.68.8  - unbound
 - 192.168.68.9  - homeassistnt
 - 192.168.68.10 - netAlertX
 - 192.168.68.12 - TV (check this is correct)
 - 192.168.68.13 - TV (check this is correct)

## The two apps in this repository
There are two simple Node.js apps managed by an [NX fraomework](https://nx.dev/).

### Frontend
  - Code is [sites/apps/frontend](./sites/apps/frontend) here
  - Stack:
    - [Angular 18](https://angular.dev/)
      - Of note, this is using the experimental zoneless setup with signals.
    - Using [bootstrap css](https://getbootstrap.com/)

### Backend
 - In [sites/apps/backend](./sites/apps/backend) here. 
 - stack:
   - [Express.js](http://expressjs.com/).

## running website locally
 
 For development I'm using homebrew and used the same database setup instructions at the bottom.

 - `brew services start postgresql@14`
 - `nx start`
 - open browser to http://localhost:4200

## infra deployment

This is an incomplete list of commands but should give you the general idea.  To update containers, just
stop all of them and bring them all back up.

 - `scp -r -p -O -P 2221 docker/db-nx.sql mfortin@station.local:/volume1/docker/nx/`
 - `scp -r -p -O -P 2221 docker/docker-compose.sh mfortin@station.local:/volume1/docker/nx/`
 - `scp -r -p -O -P 2221 docker/backend/* mfortin@station.local:/volume1/docker/nx/backend/`
 - `scp -r -p -O -P 2221 docker/frontend/* mfortin@station.local:/volume1/docker/nx/www/html/`

 - `ssh mfortin@station.local -p 2221`
 - `cd /volume1/docker/nx`
 - `sudo docker-compose up`

### infra database setup

Database setup is separate from infrastructure deployment.  But needs to happen just once.

 - `docker exec nx-postgresql -- bash TODO`
 - `create user nx with password 'password';`
 - `create database nx with owner 'nx';`
 - `\i db-nx.sql`
