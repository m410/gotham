--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11 (Homebrew)
-- Dumped by pg_dump version 14.11 (Homebrew)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: message; Type: TABLE; Schema: public; Owner: nx
--

CREATE TABLE public.message (
    id integer NOT NULL,
    title character varying(255),
    content character varying(1000)
);


ALTER TABLE public.message OWNER TO nx;

--
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: nx
--

CREATE SEQUENCE public.message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_id_seq OWNER TO nx;

--
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nx
--

ALTER SEQUENCE public.message_id_seq OWNED BY public.message.id;


--
-- Name: message id; Type: DEFAULT; Schema: public; Owner: nx
--

ALTER TABLE ONLY public.message ALTER COLUMN id SET DEFAULT nextval('public.message_id_seq'::regclass);


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: nx
--

COPY public.message (id, title, content) FROM stdin;
1	My Subject	My short content goes here
\.


--
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nx
--

SELECT pg_catalog.setval('public.message_id_seq', 1, true);


--
-- PostgreSQL database dump complete
--

