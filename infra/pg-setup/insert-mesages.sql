alter table message add column create_date timestamp not null default now();
alter table message add column age int not null default 0;


insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        22,
        'Newman Bean',
        'Qui do dolor pariatur ut aliquip dolore. Nostrud ex ea velit dolore cillum occaecat ut minim non et sunt deserunt enim qui. Sint nisi laboris proident occaecat commodo nostrud labore do. In ut pariatur laboris pariatur elit veniam laboris sint reprehenderit est Lorem amet. Id do laboris dolor sint magna irure.\r\n',
        '2020-05-05T05:51:02 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        30,
        'Nichole Leach',
        'Sint ex excepteur officia aliquip in Lorem. Occaecat qui dolore velit duis mollit. Dolore enim quis tempor elit commodo eiusmod ipsum nostrud aliqua consectetur quis et duis Lorem. Et culpa sint proident aliquip cupidatat adipisicing velit aliquip officia consectetur sint. Fugiat irure ullamco nisi velit. Fugiat ea ut aute et reprehenderit veniam ad esse in nulla anim aliqua. Ipsum elit occaecat minim laboris officia nisi sint elit elit nulla.\r\n',
        '2018-03-24T04:13:16 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        28,
        'Hilda Barrett',
        'Laboris irure sit ad anim voluptate deserunt. Culpa consectetur do excepteur nostrud eiusmod labore labore. Officia consequat eu laborum excepteur. Exercitation aliquip minim ex id sint in dolore culpa. Id dolor dolor dolor aliquip ea minim enim laboris. Ut adipisicing culpa tempor officia labore adipisicing commodo velit cillum officia occaecat labore.\r\n',
        '2024-04-02T12:25:16 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        20,
        'Gilmore Dean',
        'Reprehenderit pariatur proident est deserunt cillum cupidatat laborum pariatur nostrud dolor. Ipsum amet minim nostrud et laboris consectetur consequat occaecat do incididunt est mollit. Labore exercitation excepteur sit fugiat dolore esse voluptate occaecat minim ex labore enim enim. Enim adipisicing aute amet anim labore id adipisicing consectetur sint qui officia adipisicing ea ut. Ex incididunt do in in ex consequat incididunt quis magna irure tempor qui ipsum culpa. Enim non tempor fugiat aliquip aliquip qui.\r\n',
        '2020-05-12T07:41:10 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        31,
        'Anna Eaton',
        'Quis adipisicing ex incididunt officia sint labore elit sit pariatur quis sint dolor. Laboris sit irure et amet quis duis tempor sit duis incididunt fugiat enim voluptate. Consectetur ullamco aliqua ut aute cupidatat minim. Ea ullamco fugiat pariatur aute elit sit. Laboris tempor laboris deserunt labore nulla ut exercitation minim officia nulla esse.\r\n',
        '2022-11-15T09:50:06 +05:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        22,
        'Concepcion Stanley',
        'Eu nostrud nulla commodo ea ipsum aliquip Lorem magna Lorem veniam qui id. Lorem consectetur ex adipisicing aliqua ea adipisicing. Labore voluptate Lorem mollit duis fugiat adipisicing pariatur pariatur. Dolor et proident commodo duis magna eiusmod occaecat.\r\n',
        '2015-04-24T11:35:04 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        27,
        'Melva Puckett',
        'Deserunt mollit laboris ullamco laboris voluptate pariatur veniam reprehenderit ea id. Proident veniam consectetur ex ullamco eiusmod ipsum voluptate velit incididunt nisi quis exercitation. Nostrud ex excepteur sint duis pariatur laborum id aliquip est eu laborum.\r\n',
        '2022-04-25T12:12:39 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        29,
        'Judy Payne',
        'Eu quis qui voluptate tempor esse anim consectetur amet qui irure proident. Ut labore laboris incididunt occaecat eiusmod elit. Irure cillum est reprehenderit id duis tempor adipisicing. Incididunt minim esse incididunt aliquip consectetur ipsum Lorem. Do sunt cillum adipisicing nulla et anim sit officia culpa occaecat proident et. Ad incididunt sunt est dolor nulla. Ex reprehenderit proident Lorem irure veniam est nisi eiusmod cillum laborum pariatur aute esse duis.\r\n',
        '2014-09-19T01:30:34 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        20,
        'Brock Downs',
        'Id irure amet id sunt adipisicing nisi. Mollit non cupidatat adipisicing consequat dolor eu sit ea irure deserunt aliqua eu elit et. Quis incididunt minim aliqua labore. Do consectetur sunt elit qui mollit deserunt tempor incididunt labore anim magna sit. Veniam elit in adipisicing enim irure. Cillum esse est nulla laboris aliqua non minim non irure.\r\n',
        '2014-05-10T02:17:00 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        38,
        'Jill Chambers',
        'Nisi aute laborum esse officia anim culpa. Proident aute aute voluptate eu culpa quis cupidatat. Cupidatat ea amet in aliquip nisi irure aliqua sunt sit velit mollit. Cupidatat non eu irure sunt culpa sit tempor labore minim adipisicing sint. Ipsum laboris officia aliquip cupidatat reprehenderit sit reprehenderit sint minim dolore culpa. Non et dolor magna nisi esse id et ut culpa. Sunt sunt Lorem eu pariatur pariatur aliquip aliqua exercitation adipisicing.\r\n',
        '2014-06-27T03:19:51 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        23,
        'Patrick Powell',
        'Eu ut reprehenderit minim ea voluptate elit mollit ullamco anim. Non ex officia nulla consectetur Lorem consectetur ex qui Lorem sunt ex labore. Lorem nostrud dolor commodo et exercitation deserunt ipsum minim. Excepteur nostrud cillum sit deserunt et. Sint elit minim cupidatat occaecat laboris qui dolor esse irure irure sint tempor dolor dolore.\r\n',
        '2021-07-05T06:46:58 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        39,
        'Mooney Griffith',
        'Deserunt amet esse nulla cupidatat ullamco irure qui duis et magna fugiat non. Cupidatat ex labore labore officia fugiat nostrud ipsum proident. Voluptate quis duis ut sunt consectetur in qui proident et consectetur enim dolore est.\r\n',
        '2020-02-19T04:02:13 +05:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        35,
        'Mccormick Garner',
        'Laborum consequat sint non sint sint aliqua qui qui velit velit. Nostrud occaecat ullamco elit adipisicing consectetur adipisicing ex culpa. Laboris nostrud excepteur aliquip eiusmod occaecat elit. Proident labore esse ipsum aliqua aute excepteur laborum ipsum deserunt laboris.\r\n',
        '2024-06-07T04:34:47 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        30,
        'Kendra Guerra',
        'Ex do tempor enim amet officia eu esse incididunt ea id. Proident est id anim velit incididunt aliqua irure laboris et excepteur nisi. Tempor elit anim commodo id ea minim laboris voluptate deserunt excepteur fugiat irure id aute. Nostrud tempor aliqua irure tempor do nisi eu laboris cillum irure. Aliqua deserunt nostrud proident eu incididunt Lorem incididunt magna cupidatat amet mollit pariatur sunt anim.\r\n',
        '2019-02-15T09:49:19 +05:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        29,
        'Mcmahon Bender',
        'Cillum Lorem duis nisi ea anim ut duis et Lorem velit adipisicing eiusmod ex. Ad cillum aute sint dolor ea excepteur proident ea. Minim quis ipsum esse id qui ad id aliquip exercitation veniam dolore deserunt in aute. Non quis elit amet magna voluptate dolor mollit. Duis quis voluptate aliquip Lorem velit amet non. Aliquip fugiat ut excepteur laboris ad fugiat quis tempor sint non ut. Irure in tempor irure laborum anim excepteur mollit magna incididunt commodo.\r\n',
        '2021-10-28T07:01:26 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        22,
        'Skinner Foley',
        'Deserunt ut fugiat qui duis ad ea aliqua in anim Lorem. Incididunt quis commodo labore consequat sunt ad laboris consequat. Qui excepteur nulla non sunt voluptate nostrud laborum laborum culpa. Aliquip consequat nostrud tempor aliquip aute do ea consectetur incididunt aute magna nisi id eu.\r\n',
        '2014-06-08T08:42:43 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        23,
        'Puckett Daugherty',
        'In commodo dolor anim exercitation. Non commodo amet ullamco culpa amet nulla ullamco occaecat culpa do laboris. Fugiat ullamco magna non sunt ipsum proident laboris Lorem cillum. Dolore incididunt elit laborum ullamco nisi esse eiusmod nulla cupidatat commodo occaecat pariatur consequat anim.\r\n',
        '2021-10-07T12:49:26 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        38,
        'Vaughn George',
        'Elit ex qui sit dolor Lorem aute velit. Exercitation proident mollit minim amet nisi ipsum irure sint commodo et irure. Culpa reprehenderit culpa esse ex irure dolore tempor sint aliqua nulla excepteur id esse ex. Sit exercitation ipsum culpa magna. Sunt dolore reprehenderit occaecat aute excepteur consectetur dolore enim eiusmod exercitation amet est laboris aute.\r\n',
        '2018-11-08T09:07:09 +05:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        32,
        'Hodge Mcleod',
        'Ea in deserunt nostrud culpa occaecat exercitation ea velit adipisicing veniam nostrud et pariatur ex. Fugiat id incididunt enim duis aliqua cillum sunt culpa fugiat ut magna non in officia. Quis cupidatat ullamco veniam deserunt laborum eu dolor exercitation voluptate quis consectetur occaecat. Sit fugiat cupidatat ea amet aute officia nisi consequat irure ullamco officia labore.\r\n',
        '2022-08-05T09:29:47 +04:00'
    );

insert into
    message (
        age,
        title,
        content,
        create_date
    )
values
    (
        32,
        'Marva French',
        'Ea aute velit commodo consequat occaecat cillum nostrud commodo. Mollit in minim eu laborum consectetur consequat et esse. Incididunt adipisicing adipisicing irure veniam aliquip reprehenderit sunt eu dolor do velit ipsum voluptate adipisicing. Officia quis officia commodo laborum dolor nisi aute nisi veniam nostrud Lorem do aliquip. Adipisicing labore eiusmod exercitation nulla occaecat sit veniam ex adipisicing ex enim. Eiusmod fugiat minim ipsum culpa in occaecat id.\r\n',
        '2023-09-25T08:15:24 +04:00'
    );