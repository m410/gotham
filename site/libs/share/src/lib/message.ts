export interface Message {
  id: number;
  title: string;
  content: string;
  age: number;
  create_date: string;
}
