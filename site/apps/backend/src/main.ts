import 'tslib';
import 'reflect-metadata';
import express from 'express';
import * as path from 'path';
import { Container } from 'typedi';
import dotenv from 'dotenv';

import { MessageRepository } from './message/message.repository';

dotenv.config();

const app = express();
const port = process.env.WEB_PORT || 3333;
const host = process.env.WEB_HOST || 'localhost';
const messageRepository = Container.get(MessageRepository);

app.use('/api/assets', express.static(path.join(__dirname, 'assets')));

app.get('/api/messages', (req, res) => {
  messageRepository.list().subscribe({
    next: (msg) => res.json(msg),
    error: (err) => {
      console.error('error', err);
      res.status(500);
      res.json({status: 500, error: 'internal', message: err.message})
    },
  });
});

app.get('/api/info', (req, res) => {
  res.json({message: 'test success'});
});

const server = app.listen(port, () => {
  console.log(`Listening at http://${host}:${port}/api`);
});

server.on('error', console.error);
