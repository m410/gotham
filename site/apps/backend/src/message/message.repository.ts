import { Service } from 'typedi';
import { Observable } from 'rxjs';
import { DbClient } from '../db/db.client';
import { Message } from '@nx/share';

@Service()
export class MessageRepository {
  #query = ` select * from message; `;

  constructor(private db: DbClient) {}

  list(): Observable<Message[]> { 
    return this.db.query<Message>(this.#query);
  }
}
