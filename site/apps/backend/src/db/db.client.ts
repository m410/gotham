import { Client } from 'pg';
import dotenv from 'dotenv';
import { Service } from 'typedi';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

@Service()
export class DbClient {
  #client!: Client;

  constructor() {
    dotenv.config();

    this.#client = new Client({
      user: process.env.DB_USER || 'nx',
      host: process.env.DB_HOST || 'localhost',
      database: process.env.DB_DATABASE || 'nx',
      password: process.env.DB_PASSWORD || 'password',
      port: process.env.DB_PORT || 5432,
    });

    this.#client.connect();
  }

  query<T>(str: string): Observable<T[]> {
    return from(this.#client.query(str)).pipe(
      map((result) => result.rows as T[])
    );
  }
}
