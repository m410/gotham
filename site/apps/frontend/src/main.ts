
import { importProvidersFrom, provideExperimentalZonelessChangeDetection } from '@angular/core';
import { AppComponent } from './app/app.component';
import { MatomoModule, MatomoRouterModule } from 'ngx-matomo-client';
import { appRoutes } from './app/app.routes';
import { withEnabledBlockingInitialNavigation, provideRouter } from '@angular/router';
import { BrowserModule, bootstrapApplication } from '@angular/platform-browser';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

bootstrapApplication(AppComponent, {
    providers: [
        provideExperimentalZonelessChangeDetection(),
        importProvidersFrom(BrowserModule, MatomoModule.forRoot({
            trackerUrl: 'https://mt.4-10.us',
            siteId: '1'
        }), MatomoRouterModule.forRoot()),
        provideHttpClient(withInterceptorsFromDi()),
        provideRouter(appRoutes, withEnabledBlockingInitialNavigation())
    ]
})
  .catch((err) => console.error(err));
