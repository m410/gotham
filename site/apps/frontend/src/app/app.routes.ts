import { Route } from '@angular/router';
import { IndexComponent } from './_root/index/index.component';
import { LayoutComponent } from './_root/layout/layout.component';
import { HomeComponent } from './_root/home/home.component';
import { FourZeroFourComponent } from './_root/four-zero-four/four-zero-four.component';
import { GalleryComponent } from './gallery/gallery.component';

export const appRoutes: Route[] = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: IndexComponent,
        data: {
          focus: 'home',
          bg: 'full',
        },
      },
      {
        path: 'gallery',
        component: GalleryComponent,
        data: { focus: 'gallery' },
      },
      {
        path: 'charts',
        loadChildren: () =>
          import('./charts/charts.module').then((m) => m.ChartsModule),
        data: { focus: 'charts' },
      },
      {
        path: 'messages',
        loadChildren: () =>
          import('./messages/messages.module').then((m) => m.MessagesModule),
        data: { focus: 'messages' },
      },
      {
        path: '**',
        component: FourZeroFourComponent,
      },
    ],
  },
];
