import { NgFor } from '@angular/common';
import { Component } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';

import { MessagesService } from '../../_share/';

@Component({
    selector: 'nx-index',
    templateUrl: './index.component.html',
    styles: [],
    imports: [NgFor]
})
export class IndexComponent {
  readonly messages$ = toSignal(this.messageService.list(), {initialValue: []});

  constructor(private messageService: MessagesService) {}
  
}
