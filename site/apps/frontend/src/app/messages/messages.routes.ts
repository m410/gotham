import { Route } from '@angular/router';
import { IndexComponent } from './index/index.component';

export const messagesRoutes: Route[] = [
  {
    path: '',
    component: IndexComponent,
  },
];
