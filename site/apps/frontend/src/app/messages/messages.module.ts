import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { RouterModule } from '@angular/router';
import { messagesRoutes } from './messages.routes';
import { ShareModule } from '../_share/share.module';

@NgModule({
    imports: [
        CommonModule, 
        RouterModule.forChild(messagesRoutes), IndexComponent,
        ShareModule
    ],
})
export class MessagesModule {}
