import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesService } from './messages.service';

@NgModule({
  declarations: [
  ],
  providers: [
    MessagesService
  ],
  imports: [CommonModule],
})
export class ShareModule {}
