export interface Message {
  id: number;
  title: string;
  content: string;
  create_date: string;
  age: number;
}
