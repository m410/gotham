import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Message } from './message';

@Injectable()
export class MessagesService {
  constructor(private http: HttpClient) {}

  list(): Observable<Message[]> {
    return this.http.get<Message[]>('/api/messages');
  }
}
