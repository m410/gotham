import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterLink, RouterOutlet } from '@angular/router';
import { NgClass } from '@angular/common';
import { filter } from 'rxjs';

@Component({
    selector: 'nx-layout',
    templateUrl: './layout.component.html',
    imports: [
        RouterLink,
        NgClass,
        RouterOutlet,
    ]
})
export class LayoutComponent implements OnInit {
  focus = 'home';
  isCollapsed = false;
  showBackground = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.#navEnd();
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe({
      next: this.#navEnd
    })

  }
 
  #navEnd = () => {
    if (this.route.snapshot.firstChild?.data) {
      this.focus = this.route.snapshot.firstChild?.data['focus'];
    }
    if (this.route.snapshot.firstChild?.data) {
      const bg = this.route.snapshot.firstChild?.data['bg'] ;
      this.showBackground = (bg === 'full');
    }
  }
}
