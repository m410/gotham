import { Component } from '@angular/core';

@Component({
    selector: 'nx-index',
    templateUrl: './index.component.html',
    standalone: true,
})
export class IndexComponent {}
