import { Component } from '@angular/core';

@Component({
    selector: 'nx-four-zero-four',
    templateUrl: './four-zero-four.component.html',
    standalone: true,
})
export class FourZeroFourComponent {}
