import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
    selector: 'nx-home',
    templateUrl: './home.component.html',
    styles: [],
    imports: [RouterOutlet]
})
export class HomeComponent {}
