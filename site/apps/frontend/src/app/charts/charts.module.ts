import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { RouterModule } from '@angular/router';
import { chartRoutes } from './charts.routes';
import { ShareModule } from '../_share/share.module';

@NgModule({
    imports: [
        CommonModule,
        HighchartsChartModule,
        RouterModule.forChild(chartRoutes),
        IndexComponent,
        ShareModule
    ],
})
export class ChartsModule {}
