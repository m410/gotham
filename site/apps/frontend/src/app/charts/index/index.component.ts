import { Component, OnInit, signal } from '@angular/core';
import {map} from 'rxjs/operators';

import * as Highcharts from 'highcharts';
import { HighchartsChartModule } from 'highcharts-angular';
import { MessagesService } from '../../_share';

@Component({
    selector: 'nx-index',
    templateUrl: './index.component.html',
    imports: [HighchartsChartModule]
})
export class IndexComponent implements OnInit {
  readonly Highcharts: typeof Highcharts = Highcharts;
  readonly chartConstructor = 'chart'; // optional string, defaults to 'chart'
  readonly chartStyle = 'width: 100%; height: 400px; display: block';
  readonly chartOptions = signal<Highcharts.Options>({})

  #series1: number[] = [3, 7, 8]; 
  #series2: number[] = [5, 6, 7];
  systemError = signal('');
  view = signal(false);

  constructor(private messagesService: MessagesService) {}

 ngOnInit(): void {
  this.messagesService.list().pipe(map(ma => ma.map(m => m.age))).subscribe({
    next: messages => {
      this.chartOptions.set(this.make(messages));
      this.view.set(true);
    },
    error: err => this.systemError = err.message
  })
 }

 make(ma: number[]) {
  return {
    title: { text: 'Highcharts chart' },
    chart: {
      type: 'column'
    },
    series: [
      {
        name: 'Series 1',
        data: ma,
        zones: [
            {
              value: 7.2,
              dashStyle: 'dot',
              color: 'red',
            },
          ],
      },
      {
        name: 'Series 2',
        data: this.#series2,
      },
    ],
  } as Highcharts.Options;
}
}
