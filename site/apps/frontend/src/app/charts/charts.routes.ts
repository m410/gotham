import { Route } from '@angular/router';
import { IndexComponent } from './index/index.component';

export const chartRoutes: Route[] = [
  {
    path: '',
    component: IndexComponent,
  },
];
