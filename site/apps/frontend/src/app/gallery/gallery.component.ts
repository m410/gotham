import { Component, ViewEncapsulation } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
    selector: 'nx-gallery',
    imports: [CommonModule],
    templateUrl: './gallery.component.html',
    styles: ``,
    encapsulation: ViewEncapsulation.None
})
export class GalleryComponent {}
